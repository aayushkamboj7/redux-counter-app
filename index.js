let initialState = {
  value: 0,
};

let display = document.querySelector('.counter');

// Reducer function
function counterReducer(state = initialState.value, action) {
  switch (action.type) {
    case 'increment':
      return state + 1;

    case 'decrement':
      return state - 1;

    case 'reset':
      return 0;

    default:
      return state;
  }
}

// Create a store using createStore function
// createStore accepts reducer function

let store = Redux.createStore(counterReducer);

document.querySelector('.increment').addEventListener('click', () => {
  // dispatch is used to dispatch the action
  store.dispatch({ type: 'increment' });
});

document.querySelector('.decrement').addEventListener('click', () => {
  store.dispatch({ type: 'decrement' });
});

document.querySelector('.reset').addEventListener('click', () => {
  store.dispatch({ type: 'reset' });
});

// It listen the store for any changes
store.subscribe(() => {
  display.innerText = store.getState();
});
